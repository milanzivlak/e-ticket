import flask
from flask.blueprints import Blueprint
from datetime import datetime
from utils.db import mysql

karte_blueprint = Blueprint("karte_blueprint", __name__)

@karte_blueprint.route("/karte", methods=["GET"])
def dobavi_karte():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM karta")

    karte = cursor.fetchall()
    return flask.jsonify(karte)


@karte_blueprint.route("/karte/<int:id_karte>")
def dobavi_kartu(id_karte, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM karta WHERE id=%s", (id_karte,))
    karta = cursor.fetchone()
    #cursor.execute("SELECT * FROM linija WHERE id=%s", (karta['linijaID']))
    #linija = cursor.fetchone()
    if karta is not None:
        #karta['cijena'] = linija['cijenaLinije']
        return flask.jsonify(karta)
    else:
        return "", 404


@karte_blueprint.route("/karte", methods=["POST"])
def dodaj_kartu():
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    
    cursor.execute("SELECT * FROM linija WHERE id=%s", data["linijaID"])
    #Provjeravanje da li je karta povratna, ako jeste, nije dupla cijena nego je za 30% manja

    izabrana_linija = cursor.fetchone()
    if data["povratna"]:
        data["cijena"] = izabrana_linija["cijenaLinije"] + izabrana_linija["cijenaLinije"]*0.70
    else:
        data["cijena"] = izabrana_linija["cijenaLinije"]

    cursor.execute("SELECT * FROM korisnik WHERE id=%s", data["korisnikID"])
    izabrani_korisnik = cursor.fetchone()
    """
    Dobavljamo korisnika, pomocu njegovog tip-a dobijamo dodatni % za snizenje i koristimo ga 
    na prethono izracunatu cijenu
    """

    cursor.execute("SELECT * FROM tip WHERE id=%s", izabrani_korisnik["tipID"])
    tip_korisnika = cursor.fetchone()
    data['cijena'] -= data['cijena']*tip_korisnika["snizenje"]/100
    cijena = data['cijena']

    #Nakon toga svega, provjeravamo da li korisnik uopste moze da kupi tu kartu, odnosno da li ima dovoljno na racunu
    if izabrani_korisnik["stanje"]-data["cijena"] > 0: 
        """
        Nakon sto utvrdimo da ima dovoljno na racunu,
        provjeravamo da li ima slobodno mjesto.
        """
        if izabrana_linija["slobodnaMjesta"] > 0:
            
            cursor.execute("INSERT INTO karta(povratna, korisnikID, linijaID, cijena) VALUES(%(povratna)s, %(korisnikID)s, %(linijaID)s, %(cijena)s)", data)
            cursor.execute("UPDATE linija set slobodnaMjesta = slobodnaMjesta-1 WHERE id = %s", data["linijaID"])

            #Na kraju, skidamo sa racuna korisnika
            cursor.execute("UPDATE korisnik SET stanje = stanje - %s WHERE id = %s", (data['cijena'], data["korisnikID"]))
            db.commit()
            return flask.jsonify(flask.request.json), 201

        else:
            print("Nema vise slobodnih mjesta!")
            return "", 204

    else:
        print("Korisnik nema dovoljno iznosa na racunu!")
        return "", 204
    


@karte_blueprint.route("/karte/<int:id_karte>", methods=["PUT"])
def izmijeni_kartu(id_karte):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_karte
    cursor.execute("UPDATE karta SET povratna=%(povratna)s, korisnikID=%(korisnikID)s, linijaID=%(linijaID)s, cijena=%(cijena)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@karte_blueprint.route("/karte/<int:id_karte>", methods=["DELETE"])
def ukloni_kartu(id_karte):
    """
    Prilikom brisanja karte, povecava se broj slobodnih mjesta te linije(ukoliko je neko otkazao kartu).
    Za povecanje broja mjesta nije uradjeno ispitivanje da li je voznja istekla, iz razloga sto se ne moze koristiti linija koja je istekla.
    Takodje, racun tog korisnika se vraca na staro(ukoliko voznja nije istekla).
    """

    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM karta WHERE id=%s", (id_karte))
    karta = cursor.fetchone()
    

    cursor.execute("SELECT * FROM linija WHERE id=%s", karta['linijaID'])
    izabrana_linija = cursor.fetchone()
    cursor.execute("DELETE FROM karta WHERE id=%s", (id_karte,))

    present = datetime.now()
     #Ispitujemo da li se voznja desila ili ne, ako nije, povecava se racun korisnika i uvecavamo broj mjesta
    if izabrana_linija['vrijemePolaska'] > present:
        cursor.execute("UPDATE linija set slobodnaMjesta = slobodnaMjesta+1 WHERE id = %s", izabrana_linija['id'])
        cursor.execute("UPDATE korisnik SET stanje = stanje + %s WHERE id = %s", (karta['cijena'], karta['korisnikID']))
    else:
        print("Voznja je istekla!") #Ispis kao potvrda da je voznja istekla
    db.commit()
    return "", 204