import flask
from flask.blueprints import Blueprint

from utils.db import mysql

linije_blueprint = Blueprint("linije_blueprint", __name__)

"""
Funkcije za prikaz, dodavanje, izmjenu i brisanje linija.
"""


@linije_blueprint.route("/linije", methods=["GET"])
def dobavi_linije():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM linija")

    linije = cursor.fetchall()
    return flask.jsonify(linije)


@linije_blueprint.route("/linije/<int:id_linije>")
def dobavi_liniju(id_linije, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM linija WHERE id=%s", (id_linije,))
    linija = cursor.fetchone()
    if linija is not None:
        return flask.jsonify(linija)
    else:
        return "", 404


@linije_blueprint.route("/linije", methods=["POST"])
def dodaj_liniju():
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["vrijemePolaska"] = data["vrijemePolaska"].rstrip("Z")
    data["vrijemeDolaska"] = data["vrijemeDolaska"].rstrip("Z")
    if data["polaziste"] != data["odrediste"]:
        cursor.execute("INSERT INTO linija(vrijemePolaska, vrijemeDolaska, polaziste, odrediste, prevoznik, slobodnaMjesta, cijenaLinije) VALUES(%(vrijemePolaska)s, %(vrijemeDolaska)s, %(polaziste)s, %(odrediste)s, %(prevoznik)s, %(slobodnaMjesta)s, %(cijenaLinije)s)", data)
        db.commit()
        return flask.jsonify(flask.request.json), 201

    else:
        return "", 204


@linije_blueprint.route("/linije/<int:id_linije>", methods=["PUT"])
def izmijeni_liniju(id_linije):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_linije
    data["vrijemePolaska"] = data["vrijemePolaska"].rstrip("Z")
    data["vrijemeDolaska"] = data["vrijemeDolaska"].rstrip("Z")
    cursor.execute("UPDATE linija SET vrijemePolaska=%(vrijemePolaska)s, vrijemeDolaska=%(vrijemeDolaska)s, polaziste=%(polaziste)s, odrediste=%(odrediste)s, prevoznik=%(prevoznik)s, slobodnaMjesta = %(slobodnaMjesta)s, cijenaLinije = %(cijenaLinije)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@linije_blueprint.route("/linije/<int:id_linije>", methods=["DELETE"])
def ukloni_liniju(id_linije):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM linija WHERE id=%s", (id_linije,))
    db.commit()
    return "", 204