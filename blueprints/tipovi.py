import flask
from flask.blueprints import Blueprint

from utils.db import mysql

tipovi_blueprint = Blueprint("tipovi_blueprint", __name__)

"""
Funkcije za prikaz, dodavanje, izmjenu i brisanje tipova.
Ispitivanje da li se unosi vec postojeci tip je uradjeno u kontroleru.
"""


@tipovi_blueprint.route("/tipovi", methods=["GET"])
def dobavi_tipove():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM tip")

    tipovi = cursor.fetchall()
    return flask.jsonify(tipovi)


@tipovi_blueprint.route("/tipovi/<int:id_tipa>")
def dobavi_tipa(id_tipa, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM tip WHERE id=%s", (id_tipa,))
    tip = cursor.fetchone()
    if tip is not None:
        return flask.jsonify(tip)
    else:
        return "", 404


@tipovi_blueprint.route("/tipovi", methods=["POST"])
def dodaj_tipa():
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("INSERT INTO tip(naziv, snizenje) VALUES(%(naziv)s, %(snizenje)s)", flask.request.json)

    db.commit()
    return flask.jsonify(flask.request.json), 201


@tipovi_blueprint.route("/tipovi/<int:id_tipa>", methods=["PUT"])
def izmijeni_tipa(id_tipa):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_tipa
    cursor.execute("UPDATE tip SET naziv=%(naziv)s, snizenje = %(snizenje)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@tipovi_blueprint.route("/tipovi/<int:id_tipa>", methods=["DELETE"])
def ukloni_tipa(id_tipa):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM tip WHERE id=%s", (id_tipa,))
    db.commit()
    return "", 204