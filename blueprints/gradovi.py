import flask
from flask.blueprints import Blueprint

from utils.db import mysql

gradovi_blueprint = Blueprint("gradovi_blueprint", __name__)

"""
Funkcije za prikaz, dodavanje, izmjenu i brisanje gradova.
Ispitivanje da li se unosi vec postojeci grad je uradjeno u kontroleru.
"""

@gradovi_blueprint.route("/gradovi", methods=["GET"])
def dobavi_gradove():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM grad")

    gradovi = cursor.fetchall()
    return flask.jsonify(gradovi)


@gradovi_blueprint.route("/gradovi/<int:id_grada>")
def dobavi_grad(id_grada, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM grad WHERE id=%s", (id_grada,))
    grad = cursor.fetchone()
    if grad is not None:
        return flask.jsonify(grad)
    else:
        return "", 404


@gradovi_blueprint.route("/gradovi", methods=["POST"])
def dodaj_grad():
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("INSERT INTO grad(naziv) VALUES(%(naziv)s)", flask.request.json)

    db.commit()
    return flask.jsonify(flask.request.json), 201


@gradovi_blueprint.route("/gradovi/<int:id_grada>", methods=["PUT"])
def izmijeni_grad(id_grada):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_grada
    cursor.execute("UPDATE grad SET naziv=%(naziv)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@gradovi_blueprint.route("/gradovi/<int:id_grada>", methods=["DELETE"])
def ukloni_grad(id_grada):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM grad WHERE id=%s", (id_grada,))
    db.commit()
    return "", 204