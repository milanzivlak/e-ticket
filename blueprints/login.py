import flask
from flask import Blueprint

from utils.db import mysql

login_blueprint = Blueprint("login_blueprint", __name__)

@login_blueprint.route("/login", methods=["POST"])
def login():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM sluzbenik WHERE korisnicko_ime=%(korisnicko_ime)s AND lozinka=%(lozinka)s AND kod=%(kod)s", flask.request.json)
    sluzbenik = cursor.fetchone()
    if sluzbenik is not None:
        flask.session["sluzbenik"] = sluzbenik["korisnicko_ime"] 
        return "", 200
    else:
        return "", 404

@login_blueprint.route("/logout", methods=["GET"])
def logout():
    flask.session.pop("sluzbenik", None) # Odjava sa sistema vrsi se uklanjanjem sluzbenika iz sesije.
    return "", 200

# Funkcija kojom se dobavlja trenutno ulogovani sluzbenik.
@login_blueprint.route("/currentUser", methods=["GET"])
def current_user():
    return flask.jsonify(flask.session.get("sluzbenik")), 200