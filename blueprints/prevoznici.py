import flask
from flask.blueprints import Blueprint

from utils.db import mysql

prevoznici_blueprint = Blueprint("prevoznici_blueprint", __name__)

"""
Funkcije za prikaz, dodavanje, izmjenu i brisanje prevoznika.
Ispitivanje da li se unosi vec postojeci prevoznik je uradjeno u kontroleru.
"""

@prevoznici_blueprint.route("/prevoznici", methods=["GET"])
def dobavi_prevoznike():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM prevoznik")

    prevoznici = cursor.fetchall()
    return flask.jsonify(prevoznici)


@prevoznici_blueprint.route("/prevoznici/<int:id_prevoznika>")
def dobavi_prevoznika(id_prevoznika, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM prevoznik WHERE id=%s", (id_prevoznika,))
    prevoznik = cursor.fetchone()
    if prevoznik is not None:
        return flask.jsonify(prevoznik)
    else:
        return "", 404


@prevoznici_blueprint.route("/prevoznici", methods=["POST"])
def dodaj_prevoznika():
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("INSERT INTO prevoznik(naziv) VALUES(%(naziv)s)", flask.request.json)

    db.commit()
    return flask.jsonify(flask.request.json), 201


@prevoznici_blueprint.route("/prevoznici/<int:id_prevoznika>", methods=["PUT"])
def izmijeni_prevoznika(id_prevoznika):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_prevoznika
    cursor.execute("UPDATE prevoznik SET naziv=%(naziv)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@prevoznici_blueprint.route("/prevoznici/<int:id_prevoznika>", methods=["DELETE"])
def ukloni_prevoznika(id_prevoznika):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM prevoznik WHERE id=%s", (id_prevoznika,))
    db.commit()
    return "", 204