import flask
import datetime
from flask import Flask

# from flaskext.mysql import MySQL 
# from flaskext.mysql import pymysql 

from utils.db import mysql


from blueprints.korisnici import korisnici_blueprint
from blueprints.linije import linije_blueprint
from blueprints.gradovi import gradovi_blueprint
from blueprints.karte import karte_blueprint
from blueprints.tipovi import tipovi_blueprint
from blueprints.prevoznici import prevoznici_blueprint
from blueprints.login import login_blueprint

app = Flask(__name__, static_url_path="")


app.config["MYSQL_DATABASE_USER"] = "root" 
app.config["MYSQL_DATABASE_PASSWORD"] = "" 
app.config["MYSQL_DATABASE_DB"] = "eticket" 
app.config["SECRET_KEY"] = "ta WJoir29$"

mysql.init_app(app)                                                          
app.register_blueprint(korisnici_blueprint, url_prefix="/api")
app.register_blueprint(linije_blueprint, url_prefix="/api")
app.register_blueprint(gradovi_blueprint, url_prefix="/api")
app.register_blueprint(karte_blueprint, url_prefix="/api")
app.register_blueprint(tipovi_blueprint, url_prefix="/api")
app.register_blueprint(prevoznici_blueprint, url_prefix="/api")
app.register_blueprint(login_blueprint, url_prefix="/api")


@app.route("/")
@app.route("/index")

def index_page():
    
    return app.send_static_file("index.html")


if __name__ == "__main__":
    
    app.run("0.0.0.0", 5000, threaded=True)

