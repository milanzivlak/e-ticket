(function(angular){
    // Dobavljanje postojeceg modula app.
    var app = angular.module("app");

    
    app.controller("GradoviCtrl", ["$http", function($http) {
        var that = this; 

        this.gradovi = []; 
        
        
        this.dobaviGradove = function() {
            
            $http.get("api/gradovi").then(function(result){
                console.log(result);
                that.gradovi = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }

        this.ukloniGrad = function(id) {
            
            $http.delete("api/gradovi/" + id).then(function(response){
                console.log(response);
                that.dobaviGradove();
            },
            function(reason){
                console.log(reason);
            });
        }
        
        this.dobaviGradove();
    }]);
})(angular);