(function(angular){
    // Dobavljanje postojeceg modula app.
    var app = angular.module("app");

    
    app.controller("KarteCtrl", ["$http", function($http) {
        var that = this; 
        this.naziviGradova = [];
        this.karte = []; 
        
        
        this.dobaviKarte = function() {
            
            $http.get("api/karte").then(function(result){
                console.log(result);
                that.karte = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }

        this.ukloniKartu = function(id) {
            
            $http.delete("api/karte/" + id).then(function(response){
                console.log(response);
                that.dobaviKarte();
            },
            function(reason){
                console.log(reason);
            });
        }
        this.sviGradovi = function () {

            $http.get("api/gradovi").then(function (result) {
                console.log(result);
                that.naziviGradova = result.data
            },
                function (reason) {
                    console.log(reason);
                });
        }
        this.dobaviKarte();
        this.sviGradovi();
    }]);
})(angular);