(function (angular) {
    var app = angular.module("app");
    app.controller("LinijaFormaCtrl", ["$http", "$state", "$stateParams","$window", function ($http, $state, $stateParams, $window) {
        var that = this;

        this.novaLinija = {
            "vrijemePolaska": "",
            "vrijemeDolaska": "",
            "polaziste": 0,
            "odrediste": 0,
            "prevoznik": 0,
            "slobodnaMjesta": 0,
            "cijenaLinije": 0
            
        };

        this.trenutnoVrijeme = new Date().toISOString().slice(0, 16);
        

        this.dobaviLiniju = function(id) {
            $http.get("api/linije/" + id).then(function(result){
                
                result.data["vrijemePolaska"] = new Date(result.data["vrijemePolaska"]);
                result.data["vrijemeDolaska"] = new Date(result.data["vrijemeDolaska"]);
                
                that.novaLinija = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        this.dodajLiniju = function () {
            //Ispitujemo da li je datum vremena dolaska raniji od datuma vremena polaska.
            //Ukoliko jeste, izbacuje se alert da nije pravilno izabran datum.
            this.novaLinija["vrijemePolaska"] = new Date(this.novaLinija["vrijemePolaska"]);
            this.novaLinija["vrijemeDolaska"] = new Date(this.novaLinija["vrijemeDolaska"]);
            if (this.novaLinija["vrijemePolaska"] > this.novaLinija["vrijemeDolaska"]) {
                $window.alert("Datum dolaska ne moze biti prije datuma polaska!");
                $state.go("dodavanjeLinije");
            }else{
                $http.post("api/linije", this.novaLinija).then(function (response) {
                    console.log(response);

                    $state.go("sveLinije")
                }, function (reason) {
                    console.log(reason);
                });
            }
            
        }

        this.izmijeniLiniju = function(id) {
            $http.put("api/linije/" + id, that.novaLinija).then(function(response) {
                console.log(response);
                $state.go("linija", {"id": id});
            }, function(reason) {
                console.log(reason);
            });
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmijeniLiniju($stateParams["id"], that.linija);
            } else {
                this.dodajLiniju();
            }
        }

        if($stateParams["id"]) {
            this.dobaviLiniju($stateParams["id"]);
        }
    }]);
})(angular);