(function(angular){
    // Dobavljanje postojeceg modula app.
    var app = angular.module("app");

    
    app.controller("LinijeCtrl", ["$http", function($http) {
        var that = this; 

        this.linije = []; 
        // this.imenaGradova = [];
        
        
        this.dobaviLinije = function() {
            
            $http.get("api/linije").then(function(result){
                console.log(result);
                that.linije = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }

        this.ukloniLiniju = function(id) {
            
            $http.delete("api/linije/" + id).then(function(response){
                console.log(response);
                that.dobaviLinije();
            },
            function(reason){
                console.log(reason);
            });
        }
        // this.naziviGradova = function () {

        //     $http.get("api/gradovi").then(function (result) {
        //         console.log(result);
        //         that.imenaGradova = result.data
        //     },
        //         function (reason) {
        //             console.log(reason);
        //         });
        // }
        this.dobaviLinije();
        // this.naziviGradova();
    }]);
})(angular);