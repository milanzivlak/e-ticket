(function(angular){
    var app = angular.module("app");
    
    app.controller("PrevoznikCtrl", ["$http", "$stateParams", function($http, $stateParams) {
        var that = this;
        this.prevoznik = {};

        this.dobaviPrevoznika = function(id) {
            $http.get("api/prevoznici/" + id).then(function(result){
                that.prevoznik = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        
        this.dobaviPrevoznika($stateParams["id"]);
    }]);
})(angular)