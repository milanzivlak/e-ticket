(function(angular){
    var app = angular.module("app");
    
    app.controller("KartaCtrl", ["$http", "$stateParams", function($http, $stateParams) {
        var that = this;
        this.karta = {};
        this.naziviGradova = [];
        this.dobaviKartu = function(id) {
            $http.get("api/karte/" + id).then(function(result){
                that.karta = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        this.sviGradovi = function () {

            $http.get("api/gradovi").then(function (result) {
                console.log(result);
                that.naziviGradova = result.data
            },
                function (reason) {
                    console.log(reason);
                });
        }
        this.dobaviKartu($stateParams["id"]);
        this.sviGradovi();
    }]);
})(angular)