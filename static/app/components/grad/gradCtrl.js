(function(angular){
    var app = angular.module("app");
    
    app.controller("GradCtrl", ["$http", "$stateParams", function($http, $stateParams) {
        var that = this;
        this.grad = {};

        this.dobaviGrad = function(id) {
            $http.get("api/gradovi/" + id).then(function(result){
                that.grad = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        
        this.dobaviGrad($stateParams["id"]);
    }]);
})(angular)