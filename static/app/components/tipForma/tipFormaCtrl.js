(function (angular) {
    var app = angular.module("app");
    app.controller("TipFormaCtrl", ["$http", "$state", "$stateParams", "$window", function ($http, $state, $stateParams, $window) {
        var that = this;
        this.tipovi = [];
        this.noviTip = {
            "naziv": "",
            "snizenje": 0
        };


        this.dobaviTip = function(id) {
            $http.get("api/tipovi/" + id).then(function(result){
                that.noviTip = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        this.dodajTip = function () {
            //Ispituje se da li vec postoji tip koji se unosi.
            //Dodavanje se izvrsi ukoliko ne postoji.
            var found = false;
            for (var i = 0; i < that.tipovi.length; i++) {
                if (that.tipovi[i]['naziv'] == this.noviTip.naziv) {
                    found = true;
                }
            }
            if( found == false){
                $http.post("api/tipovi", this.noviTip).then(function (response) {
                    console.log(response);
                    $state.go("sviTipovi")
                }, function (reason) {
                    console.log(reason);
                });
            }else{
                $window.alert("Vec postoji tip koji pokusavate da unesete!");
            }
            
        }

        this.izmijeniTip = function(id) {
            //Prilikom izmjene provjeravamo da li postoji vec tip sa nazivom koji unosimo.
            var found = false;
            for (var i = 0; i < that.tipovi.length; i++) {
                if (that.tipovi[i]['naziv'] == that.noviTip.naziv) {
                    found = true;
                }
            }
            if( found == false ) {
                $http.put("api/tipovi/" + id, that.noviTip).then(function (response) {
                    console.log(response);
                    $state.go("tip", { "id": id });
                }, function (reason) {
                    console.log(reason);
                });
            }else{
                $window.alert("Vec postoji tip sa unesenim nazivom! Pokusajte ponovo sa izmjenom!")
            }
            
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmijeniTip($stateParams["id"], that.tip);
            } else {
                this.dodajTip();
            }
        }

        if($stateParams["id"]) {
            this.dobaviTip($stateParams["id"]);
        }
        this.sviTipovi = function () {

            $http.get("api/tipovi").then(function (result) {
                console.log(result);
                that.tipovi = result.data;
            },
                function (reason) {
                    console.log(reason);
                });
        }
        this.sviTipovi();
    }]);
})(angular);