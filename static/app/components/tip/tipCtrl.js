(function(angular){
    var app = angular.module("app");
    
    app.controller("TipCtrl", ["$http", "$stateParams", function($http, $stateParams) {
        var that = this;
        this.tip = {};

        this.dobaviTip = function(id) {
            $http.get("api/tipovi/" + id).then(function(result){
                that.tip = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        
        this.dobaviTip($stateParams["id"]);
    }]);
})(angular)