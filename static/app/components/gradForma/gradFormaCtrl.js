(function (angular) {
    var app = angular.module("app");
    app.controller("GradFormaCtrl", ["$http", "$state", "$stateParams","$window", function ($http, $state, $stateParams, $window) {
        var that = this;
        this.gradovi = [];
        this.noviGrad = {
            "naziv": "",
        };

        this.dobaviGrad = function(id) {
            $http.get("api/gradovi/" + id).then(function(result){
                that.noviGrad = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        this.dodajGrad = function () {
            var found = false;
            //Prolazimo kroz gradove da provjerimo da li vec postoji grad koji unosimo.
            for(var i = 0; i < that.gradovi.length; i++){
                if(that.gradovi[i]['naziv'] == this.noviGrad.naziv){
                    found = true;
                }
            }

            if( found == false){
                //Ukoliko ne postoji grad, vrsi se dodavanje novog grada.
                $http.post("api/gradovi", this.noviGrad).then(function (response) {
                    console.log(response);

                    $state.go("sviGradovi")
                }, function (reason) {
                    console.log(reason);
                });
            }else{
                $window.alert("Vec postoji grad koji pokusavate da unesete!");
            }
            
        }

        this.izmijeniGrad = function(id) {
            var found = false;
            //Prilikom izmjene provjeravamo da li postoji vec grad sa nazivom koji unosimo.
            for (var i = 0; i < that.gradovi.length; i++) {
                if (that.gradovi[i]['naziv'] == that.noviGrad.naziv) {
                    found = true;
                }
            }
            if( found == false){
                $http.put("api/gradovi/" + id, that.noviGrad).then(function (response) {
                    console.log(response);
                    $state.go("grad", { "id": id });
                }, function (reason) {
                    console.log(reason);
                });
            }else{
                $window.alert("Vec postoji grad sa unesenim nazivom! Pokusajte ponovo sa izmjenom!");
            }
            
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmijeniGrad($stateParams["id"], that.grad);
            } else {
                this.dodajGrad();
            }
        }

        if($stateParams["id"]) {
            this.dobaviGrad($stateParams["id"]);
        }
        this.sviGradovi = function () {
            //Dobavljamo gradove da bismo mogli iterisati kroz njih.
            $http.get("api/gradovi").then(function (result) {
                console.log(result);
                that.gradovi = result.data;
            },
                function (reason) {
                    console.log(reason);
                });
        }
        this.sviGradovi();
    }]);
})(angular);