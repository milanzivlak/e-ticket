(function (angular) {
    var app = angular.module("app");
    app.controller("KorisnikFormaCtrl", ["$http", "$state", "$stateParams", "$window", function ($http, $state, $stateParams, $window) {
        var that = this;
        this.korisnici = [];
        this.noviKorisnik = {
            "ime": "",
            "prezime": "",
            "kod": 0,
            "stanje": 0,
            "tipID": 0,
            
        };

        this.dobaviKorisnika = function(id) {
            $http.get("api/korisnici/" + id).then(function(result){
                that.noviKorisnik = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        this.dodajKorisnika = function () {
            var found = false;
            //Ispitujemo da li vec postoji korisnik sa unesenim kodom, ukoliko ne postoji, izvrsi se dodavanje.
            for(var i = 0; i < that.korisnici.length; i++){
                if( that.korisnici[i]["kod"] == this.noviKorisnik.kod){
                    found = true;
                }
            }

            if( found == false){
                $http.post("api/korisnici", this.noviKorisnik).then(function (response) {
                    console.log(response);

                    $state.go("korisnici")
                }, function (reason) {
                    console.log(reason);
                });
            }else{
                $window.alert("Vec postoji korisnik sa unesenim kodom! Pokusajte ponovo!");
            }
            
        }

        this.izmijeniKorisnika = function(id) {
            var found = false;
            //Prilikom izmjene takodje provjeravamo da li vec postoji korisnik sa tim kodom.
            //Medjutim, moramo jos da provjerimo da li to uopste neki drugi korisnik, jer ako ne provjeravamo id,
            //onda nece raditi promjena na ovom korisniku jer je to u stvari njegov kod.
            for (var i = 0; i < that.korisnici.length; i++) {
                if (that.korisnici[i]["kod"] == that.noviKorisnik.kod && that.korisnici[i]['id'] != that.noviKorisnik['id']) {
                    found = true;
                }
            }
            if( found == false ){
                $http.put("api/korisnici/" + id, that.noviKorisnik).then(function (response) {
                    console.log(response);
                    $state.go("korisnik", { "id": id });
                }, function (reason) {
                    console.log(reason);
                });   
            }else{
                $window.alert("Vec postoji korisnik sa unesenim kodom! Pokusajte ponovo sa izmjenom!");
            }
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmijeniKorisnika($stateParams["id"], that.korisnik);
            } else {
                this.dodajKorisnika();
            }
        }

        if($stateParams["id"]) {
            this.dobaviKorisnika($stateParams["id"]);
        }
        this.sviKorisnici = function () {

            $http.get("api/korisnici").then(function (result) {
                console.log(result);
                that.korisnici = result.data;
            },
                function (reason) {
                    console.log(reason);
                });
        }
        this.sviKorisnici();
    }]);
})(angular);