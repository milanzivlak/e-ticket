(function(angular){
    var app = angular.module("app");

    app.controller("loginCtrl", ["$http","$state", function($http, $state){
        var that = this;
        this.status = "neprijavljen"; 
        this.sluzbenik = {
            korisnicko_ime: "",
            lozinka: "",
            kod: null
        }
        
        this.ulogovan = false;

        this.login = function() {
            that.ulogovan = true;
            $http.post("api/login", this.sluzbenik).then(
                function(response) {
                    that.status = "prijavljen";
                    that.sluzbenik = {
                        korisnicko_ime: "",
                        lozinka: "",
                        kod: 0
                    }
                    
                    
                    console.log(that.ulogovan);
                    
                    $state.go('home');
                    
                    
                },
                function(reason) {
                    that.status = "neuspjesno";
                    that.sluzbenik = {
                        korisnicko_ime: "",
                        lozinka: "",
                        kod: 0
                    }
                    
                    
                }
            )
        }

        this.logout = function() {
            $http.get("api/logout").then(
                function(response) {
                    that.status = "neprijavljen";
                    that.ulogovan = false;
                    console.log(that.ulogovan);
                    $state.go("login");
                    
                },
                function(reason) {
                    console.log(reason);
                }
            )
        }

        this.getCurrentUser = function() {
            $http.get("api/currentUser").then(
                function(response) {
                    if(response.data) {
                        that.status = "prijavljen";
                        that.ulogovan = true;
                        
                    } else {
                        that.status = "neprijavljen";
                        that.ulogovan = false;
                    }
                },
                function(reason) {
                    console.log(reason);
                }
            )
        }

        this.getCurrentUser(); 
    }]);
})(angular);