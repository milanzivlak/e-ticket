(function(angular){
    var app = angular.module("app");
    
    app.controller("KorisnikCtrl", ["$http", "$stateParams", function($http, $stateParams) {
        var that = this;
        this.korisnik = {};

        this.dobaviKorisnika = function(id) {
            $http.get("api/korisnici/" + id).then(function(result){
                that.korisnik = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        
        this.dobaviKorisnika($stateParams["id"]);
    }]);
})(angular)