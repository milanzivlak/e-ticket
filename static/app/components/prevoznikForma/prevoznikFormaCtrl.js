(function (angular) {
    var app = angular.module("app");
    app.controller("PrevoznikFormaCtrl", ["$http", "$state", "$stateParams", "$window", function ($http, $state, $stateParams, $window) {
        var that = this;
        this.prevoznici = [];
        this.noviPrevoznik = {
            "naziv": "",
        };

        this.dobaviPrevoznika = function(id) {
            $http.get("api/prevoznici/" + id).then(function(result){
                that.noviPrevoznik = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        this.dodajPrevoznika = function () {
            //Ispitivanje da li vec postoji prevoznik koji se unosi.
            //Dodavanje se izvrsi ukoliko ne postoji.
            var found = false;
            for (var i = 0; i < that.prevoznici.length; i++) {
                if (that.prevoznici[i]['naziv'] == this.noviPrevoznik.naziv) {
                    found = true;
                }
            }
            if( found == false){
                $http.post("api/prevoznici", this.noviPrevoznik).then(function (response) {
                    console.log(response);

                    $state.go("sviPrevoznici")
                }, function (reason) {
                    console.log(reason);
                });
            }else{
                $window.alert("Vec postoji prevoznik koga pokusavate da unesete!");
            }
            
        }

        this.izmijeniPrevoznika = function(id) {
            //Prilikom izmjene provjeravamo da li postoji vec prevoznik sa nazivom koji unosimo.
            var found = false;
            for (var i = 0; i < that.prevoznici.length; i++) {
                if (that.prevoznici[i]['naziv'] == that.noviPrevoznik.naziv) {
                    found = true;
                }
            }
            if ( found == false){
                $http.put("api/prevoznici/" + id, that.noviPrevoznik).then(function (response) {
                    console.log(response);
                    $state.go("prevoznik", { "id": id });
                }, function (reason) {
                    console.log(reason);
                });
            }else{
                $window.alert("Vec postoji prevoznik sa unesenim nazivom! Pokusajte ponovo sa izmjenom!");
            } 
            
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmijeniPrevoznika($stateParams["id"], that.prevoznik);
                
                
            } else {
                this.dodajPrevoznika();
            }
            
        }

        if($stateParams["id"]) {
            this.dobaviPrevoznika($stateParams["id"]);
        }
        this.sviPrevoznici = function () {

            $http.get("api/prevoznici").then(function (result) {
                console.log(result);
                that.prevoznici = result.data;
            },
                function (reason) {
                    console.log(reason);
                });
        }
        this.sviPrevoznici();
    }]);
})(angular);