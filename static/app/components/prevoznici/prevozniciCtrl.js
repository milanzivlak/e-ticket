(function(angular){
    // Dobavljanje postojeceg modula app.
    var app = angular.module("app");

    
    app.controller("PrevozniciCtrl", ["$http", function($http) {
        var that = this; 

        this.prevoznici = []; 
        
        
        this.dobaviPrevoznika = function() {
            
            $http.get("api/prevoznici").then(function(result){
                console.log(result);
                that.prevoznici = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }

        this.ukloniPrevoznika = function(id) {
            
            $http.delete("api/prevoznici/" + id).then(function(response){
                console.log(response);
                that.dobaviPrevoznika();
            },
            function(reason){
                console.log(reason);
            });
        }
        
        this.dobaviPrevoznika();
    }]);
})(angular);