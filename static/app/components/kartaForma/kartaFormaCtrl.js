(function (angular) {
    var app = angular.module("app");
    app.controller("KartaFormaCtrl", ["$http", "$state", "$stateParams", "$window", function ($http, $state, $stateParams, $window) {
        var that = this;
        this.korisnici = [];
        this.linije = [];
        this.tipovi = [];
        this.novaKarta = {
            "povratna": 0,
            "korisnikID": 0,
            "linijaID": 0,
            "cijena": 0,
            
        };
        this.zavrsnaCijena;
        this.trenutnoVrijeme = new Date();
        this.polazak;
        this.dobaviKartu = function(id) {
            $http.get("api/karte/" + id).then(function(result){
                that.novaKarta = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        this.dodajKartu = function () {
            that.polazak = new Date(that.linije[this.novaKarta.linijaID-1]["vrijemePolaska"]);
            //that.zavrsnaCijena = this.novaKarta['cijena'];
            console.log("POVRATNA:" + this.novaKarta['povratna']);
            if (this.novaKarta['povratna'] == 1){
                that.zavrsnaCijena = that.linije[this.novaKarta.linijaID-1]['cijenaLinije'] + that.linije[this.novaKarta.linijaID-1]['cijenaLinije']*0.70;
            }else{
                that.zavrsnaCijena = that.linije[this.novaKarta.linijaID-1]['cijenaLinije'];
            }
            that.zavrsnaCijena = that.zavrsnaCijena - that.zavrsnaCijena*that.tipovi[that.korisnici[this.novaKarta.korisnikID-1]['tipID']]['snizenje'];
            
            //Prvo ispitujemo da li korisnik ima dovoljno sredstava za ovu transakciju.


            
            if ((that.korisnici[this.novaKarta.korisnikID]['stanje'] - that.zavrsnaCijena) < 0) {
                that.zavrsnaCijena = null;
                $window.alert("Korisnik nema dovoljno sredstava za transakciju!");
                $state.go("dodavanjeKarte");
                
            } else if(that.polazak < that.trenutnoVrijeme) {
                //Ispitujemo da li je korisnik izabrao liniju koja je istekla.
                $window.alert("Izabrana linija je istekla! Pokusajte ponovo!");
            }else{
                that.zavrsnaCijena = 0;
                $http.post("api/karte", this.novaKarta).then(function (response) {
                    console.log(response);
                    $state.go("karteKorisnika");
                    
                }, function (reason) {
                    console.log(reason);
                });
            }
            
        }

        this.izmijeniKartu = function(id) {
            $http.put("api/karte/" + id, that.novaKarta).then(function(response) {
                console.log(response);
                $state.go("karta", {"id": id});
            }, function(reason) {
                console.log(reason);
            });
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmijeniKartu($stateParams["id"], that.karta);
            } else {
                this.dodajKartu();
                
            }
        }

        if($stateParams["id"]) {
            this.dobaviKartu($stateParams["id"]);
        }

        this.sveLinije = function () {

            $http.get("api/linije").then(function (result) {
                console.log(result);
                that.linije = result.data;
            },
                function (reason) {
                    console.log(reason);
                });
        }
        this.sviKorisnici = function () {

            $http.get("api/korisnici").then(function (result) {
                console.log(result);
                that.korisnici = result.data;
            },
                function (reason) {
                    console.log(reason);
                });
        }
        this.sviTipovi = function () {

            $http.get("api/tipovi").then(function (result) {
                console.log(result);
                that.tipovi = result.data;
            },
                function (reason) {
                    console.log(reason);
                });
        }
        this.sviKorisnici();
        this.sveLinije();
        this.sviTipovi();
    }]);
})(angular);