(function(angular){
    // Dobavljanje postojeceg modula app.
    var app = angular.module("app");

    
    app.controller("TipoviCtrl", ["$http", function($http) {
        var that = this; 

        this.tipovi = []; 
        
        
        this.dobaviTipove = function() {
            
            $http.get("api/tipovi").then(function(result){
                console.log(result);
                that.tipovi = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }

        this.ukloniTip = function(id) {
            
            $http.delete("api/tipovi/" + id).then(function(response){
                console.log(response);
                that.dobaviTipove();
            },
            function(reason){
                console.log(reason);
            });
        }
        
        this.dobaviTipove();
    }]);
})(angular);