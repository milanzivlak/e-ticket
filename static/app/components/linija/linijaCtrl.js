(function(angular){
    var app = angular.module("app");
    
    app.controller("LinijaCtrl", ["$http", "$stateParams", function($http, $stateParams) {
        var that = this;
        this.linija = {};
        this.dobaviLiniju = function(id) {
            $http.get("api/linije/" + id).then(function(result){
                that.linija = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        
        this.dobaviLiniju($stateParams["id"]);
    }]);
})(angular)