(function(angular){
    
    var app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        
        $stateProvider.state({
            name: "home", 
            url: "/home", 
            views: {
                'logged': {
                    templateUrl: "app/components/kartaForma/kartaForma.tpl.html",
                    controller: "KartaFormaCtrl",
                    controllerAs: "pctrl"
                }
                 
            }
            
        }).state({
            name: "dodavanjeKorisnika",
            url: "/korisnikForma",
            views: {
                'logged': {
                    templateUrl: "app/components/korisnikForma/korisnikForma.tpl.html",
                    controller: "KorisnikFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "korisnici",
            url: "/korisnici",
            views: {
                'logged': {
                    templateUrl: "app/components/korisnici/korisnici.tpl.html",
                    controller: "KorisniciCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "korisnik",
            url: "/korisnici/{id}",
            views: {
                'logged':{
                    templateUrl: "app/components/korisnik/korisnik.tpl.html",
                    controller: "KorisnikCtrl",
                    controllerAs: "pctrl"
                }
            } 
            
        }).state({
            name: "izmjenaKorisnika",
            url: "/korisnikForma/{id}",
            views: {
                "logged": {
                    templateUrl: "app/components/korisnikForma/korisnikForma.tpl.html",
                    controller: "KorisnikFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
            
            //------------------------------------------------------------------------------

        }).state({
            name: "dodavanjeKarte",
            url: "/kartaForma",
            views: {
                'logged': {
                    templateUrl: "app/components/kartaForma/kartaForma.tpl.html",
                    controller: "KartaFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "karteKorisnika",
            url: "/karte",
            views: {
                'logged': {
                    templateUrl: "app/components/karte/karte.tpl.html",
                    controller: "KarteCtrl",
                    controllerAs: "pctrl"
                }
            }
        }).state({
            name: "karta",
            url: "/karta/{id}",
            views: {
                'logged': {
                    templateUrl: "app/components/karta/karta.tpl.html",
                    controller: "KartaCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "izmjenaKarte",
            url: "/kartaForma/{id}",
            views: {
                'logged': {
                    templateUrl: "app/components/kartaForma/kartaForma.tpl.html",
                    controller: "KartaFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
            
            //--------------------------------------------------------------------------

        }).state({
            name: "dodavanjeGrada",
            url: "/gradForma",
            views: {
                'logged': {
                    templateUrl: "app/components/gradForma/gradForma.tpl.html",
                    controller: "GradFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "sviGradovi",
            url: "/gradovi",
            views: {
                'logged': {
                    templateUrl: "app/components/gradovi/gradovi.tpl.html",
                    controller: "GradoviCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "grad",
            url: "/grad/{id}",
            views: {
                'logged': {
                    templateUrl: "app/components/grad/grad.tpl.html",
                    controller: "GradCtrl",
                    controllerAs: "pctrl"
            }
        }
        }).state({
            name: "izmjenaGrada",
            url: "/gradForma/{id}",
            views: {
                'logged': {
                    templateUrl: "app/components/gradForma/gradForma.tpl.html",
                    controller: "GradFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
            //----------------------------------------------------------------------------

        }).state({
            name: "dodavanjePrevoznika",
            url: "/prevoznikForma",
            views: {
                'logged': {
                    templateUrl: "app/components/prevoznikForma/prevoznikForma.tpl.html",
                    controller: "PrevoznikFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "sviPrevoznici",
            url: "/prevoznici",
            views: {
                'logged': {
                    templateUrl: "app/components/prevoznici/prevoznici.tpl.html",
                    controller: "PrevozniciCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "prevoznik",
            url: "/prevoznik/{id}",
            views: {
                'logged': {
                    templateUrl: "app/components/prevoznik/prevoznik.tpl.html",
                    controller: "PrevoznikCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "izmjenaPrevoznika",
            url: "/prevoznikForma/{id}",
            views: {
                'logged': {
                    templateUrl: "app/components/prevoznikForma/prevoznikForma.tpl.html",
                    controller: "PrevoznikFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
            
            //----------------------------------------------------------------------------
            
        }).state({
            name: "dodavanjeTipa",
            url: "/tipForma",
            views: {
                'logged': {
                    templateUrl: "app/components/tipForma/tipForma.tpl.html",
                    controller: "TipFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
        }).state({
            name: "sviTipovi",
            url: "/tipovi",
            views: {
                'logged': {
                    templateUrl: "app/components/tipovi/tipovi.tpl.html",
                    controller: "TipoviCtrl",
                    controllerAs: "pctrl" 
            }
        }
        }).state({
            name: "tip",
            url: "/tip/{id}",
            views: {
                'logged': {
                    templateUrl: "app/components/tip/tip.tpl.html",
                    controller: "TipCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "izmjenaTipa",
            url: "/tipForma/{id}",
            views: {
                'logged': {
                    templateUrl: "app/components/tipForma/tipForma.tpl.html",
                    controller: "TipFormaCtrl",
                    controllerAs: "pctrl"       
            }}
            //----------------------------------------------------------------------------
        }).state({
            name: "dodavanjeLinije",
            url: "/linijaForma",
            views: {
                'logged': {
                    templateUrl: "app/components/linijaForma/linijaForma.tpl.html",
                    controller: "LinijaFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
        }).state({
            name: "sveLinije",
            url: "/linije",
            views: {
                'logged': {
                    templateUrl: "app/components/linije/linije.tpl.html",
                    controller: "LinijeCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "linija",
            url: "/linija/{id}",
            views: {
                "logged": {
                    templateUrl: "app/components/linija/linija.tpl.html",
                    controller: "LinijaCtrl",
                    controllerAs: "pctrl"
                }
            }
            
        }).state({
            name: "izmjenaLinije",
            url: "/linijaForma/{id}",
            views: {
                'logged':{
                    templateUrl: "app/components/linijaForma/linijaForma.tpl.html",
                    controller: "LinijaFormaCtrl",
                    controllerAs: "pctrl"
                }
            }
            //----------------------------------------------------------------------------;
        }).state({
            name: "login",
            url: "/login",
            views: {
                'log': {
                    templateUrl: "app/components/login/login.tpl.html",
                    controller: "loginCtrl",
                    controllerAs: "lctrl"
                }
                
            }
            
        });

        
        $urlRouterProvider.otherwise("/login")
    }]);
    app.run(function ($rootScope, $location) {
        $rootScope.location = $location;
    });
})(angular);